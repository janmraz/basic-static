/**
 * Created by janmraz on 29/10/2016.
 */
module.exports = {
    development: {
        port: process.env.PORT || 8080,
        mongo_url: 'mongodb://127.0.0.1/backend',
        secret: 'secretBitches',
        callbackURL: 'http://localhost:8080/api/user/facebook/auth/callback'
    },
    production: {
        port: process.env.PORT || 8080,
        mongo_url: 'mongodb://rm15:rm15rm15rm15@ds111754.mlab.com:11754/database123456',
        secret: 'secretBitches',
        callbackURL: 'http://taskmanager7.herokuapp.com/api/user/facebook/auth/callback'
    },
    clientID: '1851001551787668',
    clientSecret: '359fc87d2b3ebf7a85d5596b268b0648',
    pushNotificationPathToKey: '',
    pushNotificationKeyId: '',
    pushNotificationTeamId: '',
    spark_api: 'cbc7847d474a1becc709179b6bd58422c80c284a'
};
