'use strict';

const cookieParser = require('cookie-parser');

const cors = require('cors');

module.exports = function (app) {

    // use cookierParser
    app.use(cookieParser());

    // allow requests from cross origin
    app.use(cors());

    let  path = require('path');
    app.get('/privacy',function (req, res) {
        res.sendFile(path.resolve('public/html/privacy.html'));
    });
    app.get('/terms',function (req, res) {
        res.sendFile(path.resolve('public/html/terms.html'));
    });
};
