const express = require('express');
const app = express();
const morgan = require('morgan');
const env = process.env.NODE_ENV || 'development';
var bodyParser = require('body-parser');
app.use(bodyParser.urlencoded({ extended: false }));

// parse application/json
app.use(bodyParser.json());


app.use(express.static('public'));

// use morgan for logging errors
app.use(morgan('tiny'));

app.set('port', process.env.PORT || 8081);

// routes
const routes = require('./routes.js');
routes(app);

app.listen(app.get('port'), function() {
    console.log('Express server listening on port', app.get('port'));
});
